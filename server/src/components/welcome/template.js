const template = (context) => {
  return new Promise((resolve) => {
    resolve(`
      <h5 class="card-title">Before you can proceed...</h5>
      <div class="card-text">
        <p>
          Please remember that results of the checker should not be taken as an official medical opinion and 
          it is best to seek professional help. This is an endeavour to simplify life in times of emergency 
          just to gain some clarity and should be taken with a grain of salt.  
        </p>
        <p>
          Click Next to move to the first question.
        </p>
      </div>
    `);
  });
};

export default template;
