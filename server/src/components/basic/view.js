import View from '../../base/view';
import template from './template';

export default class BasicView extends View {
  constructor(el, context) {
    const handleSexChange = (e) => {
      this.context.patient.setSex('male');
    };

    const handleAgeChange = (e) => {
      this.context.patient.setAge(30);
    };

    const binds = {
      '.input-sex': {
        type: 'change',
        listener: handleSexChange
      },
      '#input-age': {
        type: 'change',
        listener: handleAgeChange
      }
    };

    super(el, template, context, binds);
    this.context.patient.reset();
  }
}
