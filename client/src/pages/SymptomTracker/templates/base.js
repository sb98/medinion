

const template = `
  <div class="card">
      <div class="card-header text-light bg-dark"> 
          MEDINION  
      </div>
      <div id="step-container" class="card-block">
        <!-- dynamic content goes here -->
      </div>
      <div class="card-footer">
          <button id="next-step" class="btn btn-success float-right">Next <i class="fa fa-chevron-right"></i></button>
      </div>
  </div>
`;

export default template;
