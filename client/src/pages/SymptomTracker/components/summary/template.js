

import html from '../../templates/helpers';

const conditionsHtmlMapper = (conditions) => {
  return conditions.map((condition) => `
    <div class="summary-item row">
      <div class="col-8">
        ${condition.name}
      </div>
      <div class="col-4">
        <div class="progress">
          <div class="progress-bar bg-success" role="progressbar" 
              style="width: ${Math.floor(condition.probability * 100)}%">
            ${Math.floor(condition.probability * 100)}%
          </div>
        </div>
      </div>
      <div class="explanation col-12"></div>
    </div>          
  `);
};

const template = (context) => {
  return context.api.diagnosis(context.patient.toDiagnosis()).then((data) => {
    return html`
      <h5 class="card-title">Summary</h5>
      <div class="card-text">
        <p>Based on the interview, you could suffer from:</p>
        ${conditionsHtmlMapper(data.conditions)}
        <div class="alert alert-warning" role="alert">
          <i class="fa fa-info-circle"></i>
          Please note that the list below may not be complete and is provided solely for informational purposes 
          and is not a qualified medical opinion. Click on the back button of your browser to go back to Home Page
        </div>
      </div>
    `;
  });
};

export default template;
