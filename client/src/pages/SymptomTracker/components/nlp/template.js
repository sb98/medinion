

import html from '../../templates/helpers';

const template = (context) => {
  return new Promise((resolve) => {
    resolve(html`
      <h5 class="card-title">Enter symptoms below</h5>
      <div class="card-text">
        <form>
          <div class="form-group">
            <textarea placeholder="e.g. I got headache" class="form-control" id="input-feel" rows="4"></textarea>
          </div>
        </form>
        <p>Identified observations:</p>
        <ul class="list-unstyled" id="observations">
        </ul>
        <p class="text-muted small">
          <i class="fa fa-exclamation-circle"></i>
          Please note that you can go Next only if there are 
          <span class="text-success">present <i class="fa fa-plus-circle"></i></span>
          identified observations.
        </p>
        <p class="text-muted small">
          <i class="fa fa-info-circle"></i>
          All of the identified symptoms will be added after clicking Next.
        </p>
      </div>
    `);
  });
};

export default template;
