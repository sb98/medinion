import React, {Component} from 'react';
import { Jumbotron, Container, Col, Form, Button, } from 'react-bootstrap';
class SearchHsptl extends Component{
    render(){
    return(
<>
<Jumbotron fluid className='text-light bg-dark'>
  <Container>
  <h1>Search for Hospitals!</h1>
        <Form method = "get" title = "Search Form" action="https://cse.google.com/cse/publicurl">
        <Form.Row>
              <Col xs={12} md={8}>
                <Form.Control
                  name='q'
                  id='q'
                  title='Search this site'
                  type='text'
                  size='lg'
                  placeholder='Eg- Hospitals in Kolkata/Hospitals near me etc'
                  alt='Search Text'
                />
                <Form.Control 
                type='hidden'
                id='cx'
                name='cx'
                value='a2aa3a5037312e376'/>
              </Col>
              <Col xs={12} md={4}>
                <Button type='submit' variant='success' size='lg'>
                  Submit Search
                </Button>
              </Col>
            </Form.Row>

</Form>
</Container>
</Jumbotron>
</> 
    );
    }
};
export default SearchHsptl;