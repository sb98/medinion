import React, { useState } from 'react';
import { Jumbotron, Container, Col, Form, Button, Card, CardColumns } from 'react-bootstrap';

import {searchMed } from '../utils/API';


const SearchMeds = () => {
  // create state for holding returned api data
  const [searchedMedicines, setSearchedMedicines] = useState([]);
  // create state for holding our search field data
  const [searchInput, setSearchInput] = useState('');

  const handleFormSubmit = async (event) => {
    event.preventDefault();

    if (!searchInput) { 
      return false;
    }
    try {
      const response = await searchMed(searchInput);
      console.log(response);
      const medData = response.results.map((med) => ({
        a_no: med.application_number,
        sponsor: med.sponsor_name,
        pd: med.products.map ((data) =>({
           name: data.brand_name,
           dosage: data.dosage_form,
           route: data.route,
           composition: data.active_ingredients.map((i)=>({
             comp: i.name,
             strength: i.strength
           }))
        }))
      }));
    
      setSearchedMedicines(medData);
      setSearchInput('');
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Jumbotron fluid className='text-light bg-dark'>
        <Container>
          <h1>Search for Medicines!</h1>
          <Form onSubmit={handleFormSubmit}>
            <Form.Row>
              <Col xs={12} md={8}>
                <Form.Control
                  name='searchInput'
                  value={searchInput}
                  onChange={(e) => {setSearchInput(e.target.value)} }
                  type='text'
                  size='lg'
                  placeholder="Look up a medicine now!"
                />
              </Col>
              <Col xs={12} md={4}>
                <Button type='submit' variant='success' size='lg'>
                  Submit Search
                </Button>
              </Col>
            </Form.Row>
          </Form>
        </Container>
      </Jumbotron>

      <Container>
        <h5>
          {searchedMedicines.length
            ? `Results:`
            : 'Nothing is here!'}
        </h5>
          {searchedMedicines.map((i) => {
            return(
              <>
              <h5>Sponsor: {i.sponsor} </h5>
              <CardColumns>
              {i.pd.map((med, ind) =>{
                return (
                  <>
                  <Card border='success' key={ind} className='text-light bg-dark' >
                    <Card.Body>
                      <Card.Title>{med.name}</Card.Title>
                      <Card.Text>Dosage: {med.dosage} </Card.Text>
                      <Card.Text>Route: {med.route} </Card.Text>
                      <Card.Text>Active_ingredients: {med.composition.map((j)=>{
                            return(
                              <>{j.comp}, {j.strength}</>
                            )
                          })}</Card.Text>
                    </Card.Body>
                  </Card>
                  </>
                );
              })}
              </CardColumns>
             </> 
            );
          }
          )}
          </Container>
    </>
  );
};

export default SearchMeds;
