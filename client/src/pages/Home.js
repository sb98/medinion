import React, {Component} from 'react';
import { Jumbotron, Container } from 'react-bootstrap';
class HomePage extends Component{
    render(){
    return(
<>
<Jumbotron fluid className='text-dark bg-white border border-success border border-3'>
        <Container >
          <h1 class='text-center'>Welcome to Medinion!</h1>
          <p>Medinion aims to be your companion during medical emergencies.
            Here, you can track your symptoms to reach a conclusion, look up medicines for 
            their compositions, dosage and much more and also find hospitals/clinics near
            your current location. All things said, please remember that the best option is
            always to seek professional help in person and the results from here are not 
            absolute or completely error free.  
          </p>
          <h3 class='text-center'>Track Symptoms!</h3>
          <p class='text-center'>
              <a href='http://127.0.0.1:8080/' class='text-success'>Click here</a> to answer some questions and track your symptoms.
          </p>
          <h3 class='text-center'>Explore More!</h3>
          <p class='text-center'>Refer to the navigation bar above to explore more features.</p>
        </Container>
      </Jumbotron>
      <Container>
      <p class='text-secondary fs-6 text-center'>Created by Group-14, 4th Year, Department of Computer Science and Engineering, STCET (2021)</p>
      </Container>
</> 
    );
    }
};
export default HomePage;