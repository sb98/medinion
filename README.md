cd to root directory and then execute the commands below

cd medinion-master
npm install
cd client
npm install
cd ..
cd server
npm install
cd ..
cd client
yarn start

Now open another terminal window and cd to medinion-master and follow the commands below
cd server
yarn dev

The application will run at localhost:3000